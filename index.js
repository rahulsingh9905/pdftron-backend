const express = require('express');
const bodyParser = require('body-parser');
const annotationHandler = require('./annotationHandler');

const app = express();

app.use(bodyParser.text());

annotationHandler(app);

// Run server
app.listen(8000, 'localhost', (err) => {
	if (err) {
		console.error(err);
	} else {
    console.info(`Server is listening at http://localhost:8000/`);
  	}
});